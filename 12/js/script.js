(() => {
    'use strict';
    function Phone() {
        function first(charIndex) {
            console.log(this);
           // console.log(this.title[charIndex]);
        }
        let x = 'Hello';
        this.title = 'No Name';
        this.print = () => {
            console.log(this);
            /*first.apply(this, [1]);
            first.call(this, 0);
            first.bind(this)(4);*/
            first(0);
            console.log(x);
            console.log(this.title);
        };
        return this;
    }

    let phone = new Phone;
    console.log(phone.title);
    phone.print();
   // phone.first();


    function test() {
        let a = 'Test';
        return function () {
            this.echo = () => {
                console.log(a);
            };
        };
    }

    let func = test();
    let obj = new func();
    obj.echo();

    function PC() {
        let first = () => {
            console.log(this);
        };
        return {
            title: "PC",
            print() {
                first();
                console.log(this.title);
            }
        };
    }

    let pc = new PC();
    pc.print();

    /*function Animal() {

    }
    Animal.prototype.info = function () {
        console.log(this.name);
    };
    function Rabbit() {
        this.name = "Rabbit 1";
    }
    Rabbit.prototype = Object.create(Animal.prototype);

    let rabbit = new Rabbit();
    rabbit.info();*/

    class Animal {
        constructor(name) {
            this.name = name;
        }
        getName() {
            return this.name;
        }
    }
    function privateFunc() {
        console.log(this);
    }
    class Cat extends Animal {
        constructor(name, lifeCount = 9) {
            super(name);
            this.lifeCount = lifeCount;
        }
        getName() {

        }
        dead() {
            this.lifeCount--;
        }
        getLifeCount() {
            privateFunc.bind(this)();
            return this.lifeCount;
        }
    }
    let animal = new Animal('Animal');
    console.log(animal.getName());
    let mashaCat = new Cat('Masha');
    let sergeyCat = new Cat('Sergey');
    console.log(sergeyCat.getName());
    console.log(sergeyCat.getLifeCount());
    console.log(mashaCat.getName());
    console.log(mashaCat.getLifeCount());
    mashaCat.dead();
    console.log(mashaCat.getLifeCount());
    console.log(sergeyCat.getLifeCount());
    console.log(mashaCat.privateName);
})();








