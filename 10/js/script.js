(function () {
    'use strict';
    function l(...args) {
        console.log(...args);
    }
    function sayHello() {
        alert('Hello');
        //alert();
    }
    l(1, 2, 3, 4);

    function alert(message) {
        sayHello();
    }

    let func = function menu(num) {
        l(num);
        if (num < 10) {
            menu(num + 1);
        }
    }
    let anyFunc = func;
    func = null;
    // func(1);
    anyFunc(1);

    const hello = (text) => l(text);
    hello('Hello');

    let array = [1, 2, 3, 4, 5];

    array = array.map(function (item) {
        return Math.pow(item, 2);
    });
    l(array);
    array = array.map(item => Math.pow(item, 2));
    l(array);
    array = array.map(Math.sqrt);
    l(array);
    let name = 'Petya';
    let str = `Hello ${name}.               My name is Eugene.`;
    let div = document.createElement('div');
    div.innerHTML = str;
    document.body.appendChild(div);
    l(str);
})();






