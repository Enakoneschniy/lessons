(() => {
    'use strict';
    let app = document.getElementById('app');
    let linkWrapper = document.createElement('div');
    linkWrapper.className = 'box';
    let link = document.createElement('a');
    link.innerText = "I'm link";
    link.href = 'http://google.com';

    linkWrapper.addEventListener('click', (event) => {
        alert('Click to Black Box!');
    });
    document.body.addEventListener('contextmenu', (event) => {
        event.preventDefault();
    });

    link.addEventListener('click', (event) => {
        event.preventDefault();
        event.stopPropagation();
        let href = event.target.getAttribute('href');
        if(confirm('Вам есть 18 лет?')) {
            window.open(href);
        }

    });
    linkWrapper.appendChild(link);
    app.appendChild(linkWrapper);

    document.getElementById('input-1')
        .addEventListener('keydown', (event) => {
            console.log(event.key,':' ,event.target.value);
        });
    document.getElementById('input-2')
        .addEventListener('keyup', (event) => {
            console.log(event.key,':' ,event.target.value);
        });
    document.getElementById('input-3')
        .addEventListener('keypress', (event) => {
            console.log(event.key,':' ,event.target.value);
        });
    document.getElementById('input-4')
        .addEventListener('input', (event) => {
            console.log(event.key,':' ,event.target.value);
        });

})();