(() => {
    function privateFunction() {
        console.log(this);
    }
    class Animal {
       constructor(name, age) {
            this.name = name;
            this.age = age;
       }
       info() {
           let keys = Object.keys(this);
           let strResult = '';
           keys.forEach(key => {
               strResult += `${key}: ${this[key]} `;
           });
           privateFunction.bind(this)();
           return strResult.trim();
       }
       toString() {
           let keys = Object.keys(this);
           let strResult = '';
           keys.forEach(key => {
               strResult += `${key}: ${this[key]} `;
           });
           return strResult.trim();
       }
    }
    let animal = new Animal('Animal', 12);
    console.log(animal);

    class Dog extends Animal {
        constructor(name, age, breed) {
            super(name, age);
            this.breed = breed;
            this.color = 'red';
        }
        getName() {

        }
        setName(value) {
            if(typeof value !== 'string') {
                throw new Error("Invalid Name");
            } else {
                this.name = value;
            }
        }
    }
    let dog = new Dog('Anatoliy', 2, 'pitbull');
    try {
        dog.setName(23423);
        console.log('sdfsdfsdf');
    } catch (e) {
        alert(e.message);
        console.log(window.location.href);
        // window.location.reload();
    }

    console.log(dog.info());

    class Fish extends Animal {
        constructor(name, age, scales) {
            super(name, age);
            this.scales = scales;
        }
    }

    let fish = new Fish('Vobla', 1, true);
    console.log(fish.name);


})();








