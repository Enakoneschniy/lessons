(() => {
    /*navigator.getUserMedia = navigator.getUserMedia ||
        navigator.webkitGetUserMedia ||
        navigator.mozGetUserMedia;

    if (navigator.getUserMedia) {
        navigator.getUserMedia({ audio: true, video: { width: 1280, height: 720 } },
            function(stream) {
                var video = document.querySelector('video');
                video.srcObject = stream;
                video.onloadedmetadata = function(e) {
                    video.play();
                };
            },
            function(err) {
                console.log("The following error occurred: " + err.name);
            }
        );
    } else {
        console.log("getUserMedia not supported");
    }*/
    /*if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(position => {
            console.log(position);
        });
    }*/

    function scrollTo(element, to, duration) {
        if (duration <= 0) return;
        let difference = to - element.scrollTop;
        let perTick = difference / duration * 10;

        setTimeout(function() {
            element.scrollTop = element.scrollTop + perTick;
            if (element.scrollTop === to) return;
            scrollTo(element, to, duration - 10);
        }, 10);
    }
    let backgrounds = ['red', 'green', 'blue', 'yellow', 'purple'];
    let app = document.getElementById('app');

    let navigation = document.createElement('ul');
    let blocks  = document.getElementById('blocks');
    navigation.className = 'main-nav';

    for(let num = 1; num <= 5; num++) {
        let item = document.createElement('li');
        item.className = 'main-nav-item';
        let link = document.createElement('a');
        link.href = `#page${num}`;
        link.innerHTML = `Page ${num}`;
        link.onclick = (e) => {
            // e.preventDefault();
            e.stopPropagation();
            let href = e.target.getAttribute('href');
            //alert("Scroll to: " + e.target.innerText);
        };
        link.addEventListener('dblclick', e => {
            e.stopPropagation();
            let href = e.target.getAttribute('href');
            alert("Event Listener : " + e.target.innerText);
        });
        item.appendChild(link);
        navigation.appendChild(item);

        let block = document.createElement('div');
        block.className = `block ${backgrounds[num - 1]}`;
        block.setAttribute('id', `page${num}`);
        blocks.appendChild(block);
    }


    app.appendChild(navigation);

})();







