(() => {
    String.prototype.reverse = function () {
        return this.split('').reverse().join('');
    };
    Array.prototype.sum = function() {
        return this.reduce((sum, current) => sum += current, 0);
    };
    let array = [1, 2, 3, 4, 5];
    console.log(array.sum());
    console.log(array);
    array = array.map(item => Math.pow(item, 3));
    console.log(array);
    array = array.filter(item => item % 2 === 0);
    console.log(array);
    array.forEach(item => console.log(item));
    let sum = array.reduce((summ, current) => summ += current, 0);
    console.log(sum);
    let mul = array.reduce((mult, current) => mult *= current, 1);
    console.log(mul);

    let names = 'Маша, Петя, Марина, Василий';
    let arr = names.split(', ');
    console.log(arr);
    arr = arr.reverse();
    let str = arr.join(', ');
    console.log(str.reverse());
    let arr1 = ["Я", "сейчас", "изучаю", "JavaScript"];

    arr1.splice(0, 1, "Мы", "изучаем");
    console.log(arr1);

    let pos = arr1.indexOf('изучаю');
    console.log(pos);
    arr1.splice(pos, 1, 'новый');
    console.log(arr1);
})();











