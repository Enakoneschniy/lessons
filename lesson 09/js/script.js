'use strict';

let arrNumbers = [12, 34, 56];

console.log(arrNumbers[1]);

arrNumbers.push(454);

console.log(arrNumbers);

arrNumbers.unshift(111);

console.log(arrNumbers);

arrNumbers.push(222, 444);

console.log(arrNumbers);

let arrNumbers1 = [1, 2, 3, 4, 5];

arrNumbers.push(...arrNumbers1); // arrNumbers = arrNumbers.concat(arrNumbers1);

console.log(arrNumbers);


console.log(arrNumbers.shift());
console.log(arrNumbers);

console.log(arrNumbers.pop());
console.log(arrNumbers);

arrNumbers.splice(3, 1);

console.log(arrNumbers);

arrNumbers1 = arrNumbers.slice(2, 4);

console.log(arrNumbers, arrNumbers1);

// delete arrNumbers[3];
arrNumbers.push('sdfsad');
arrNumbers.push(true);
arrNumbers.push({});
arrNumbers.push([]);

console.log(arrNumbers);


let matrixMultiply = [];
const n = 10;

for (let row = 1; row <= n; row++) {
    matrixMultiply[row - 1] = [];
    for (let cell = 1; cell <= n; cell++) {
        matrixMultiply[row - 1][cell - 1] = row * cell;
    }
}

console.log(matrixMultiply);
let matrixMultiply1 = [];
for (let row = 0; row < n; row++) {
    matrixMultiply1[row] = [];
    for (let cell = 0; cell < n; cell++) {
        matrixMultiply1[row][cell] = (row + 1) * (cell + 1);
    }
}
console.log(matrixMultiply1);

let mt10 = [];


for (let row = 0; row < n; row++) {
    mt10[row] = [];
    for (let cell = 0; cell < n; cell++) {
        if (row === cell) {
            mt10[row][cell] = 1;
        } else if (n - 1 - cell === row) {
            mt10[row][cell] = 2;
        } else if (row > cell && n - 1 - cell > row) {
            mt10[row][cell] = 6;
        } else if (row < cell && n - 1 - cell > row) {
            mt10[row][cell] = 3;
        } else if (row > cell && n - 1 - cell < row) {
            mt10[row][cell] = 5;
        } else {
            mt10[row][cell] = 4;
        }
    }
}
console.log(mt10);

function genMatrix(n) {
    let matrixMultiply = [];

    for (let row = 1; row <= n; row++) {
        matrixMultiply[row - 1] = [];
        for (let cell = 1; cell <= n; cell++) {
            matrixMultiply[row - 1][cell - 1] = row * cell;
        }
    }
    return matrixMultiply;
}

let matrix = genMatrix(10);
let matrix1 = genMatrix(5);

console.log(matrix);
console.log(matrix1);


function sum(a, b = 8) {
    return a + b;
}

console.log(sumAll(1, 1, 54, 57, 65));

function sumAll(...arg) {
    let sum = 0;
    /*for (let i = 0; i < arg.length; i++) {
        sum += arg[i];
    }*/
    /*for(let i in arg) {
        sum += arg[i];
    }*/
    for(let val of arg) {
       sum += val;
    }
    /*arg.forEach(function (element) {
        sum += element;
    });*/
    return sum;
}

let func = function () {
    console.log('func');
};
func();

(function () {
    console.log('HELLO!!!');
    function test() {
        console.log('blablabla');
    }
    test();
})();
(function () {
    console.log('HELLOsdddd!!!');
    function test() {
        console.log('blablasdasdasdasabla');
    }
    test();
})();





